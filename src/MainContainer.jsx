import styled from 'styled-components';

export const MainContainer = styled.div`
  background-color: #999;

  .aside {
    background: #828f9d;
    width:100px;
    height:100vh;
    margin:0;
    padding:0;
    
  }
  .aside ul {
    margin:0;
    padding:0;
  }
  .aside ul li {
    list-style:none;
  }
  .aside a {
    text-decoration:none;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 15px;
    justify-content:center;
    height:80px;
  }
  .aside span {
    font-size:14px;
    color:#fff;  
  }
  .main-box {
    background: #f6f6f6;
    padding:30px 30px;
  }
  .header-main {
    display: flex;
    justify-content: space-between;
    flex-wrap:wrap;
  }
  .header-main h2 {
    color: #918b8b;
  }
  .selects-header {
    display: flex;
    align-items: center;
    gap: 10px;
    flex-wrap:wrap;

  }
  .selects-header div {
    border-radius: 20px;
    padding: 10px 30px;

    display: flex;
    align-items: center;
    gap: 20px;
  }
  .card-main {
    display: flex;
    gap: 10px;
    margin-top:40px;
    flex-wrap:wrap;
  }
  .card-main > div {
    display: flex;
    flex-direction:row;
    align-items: center;
    justify-content: start;
    background: #fff;
    height:120px;
    gap:20px;
    flex:1;
  }
  .card-main-item {
    display: flex;
  }
  .card-main-item-text p {
    color:#918b8b;
    font-size:14px;
    font-weight:bold;
  }
  .card-main-item-text span {
    font-size:20px;
    color:#918b8b;
  }
  .card-main-item.separate {
    background-color:#f6f6f6;
    display: flex;
    flex-direction:column;
    
  }
  .card-main-item.separate > div {
    background-color:#fff;
    flex:1;
    width:100%;
    color:#918b8b;
  }
  .card-main.card-two-separate > div{
    height:unset;
  }
  .separate-items {
    display: flex;
    flex-wrap:wrap;
    justify-content:space-between;
  }
  .card-three {
    margin-top:40px;


  }
  .card-three > div {
    background: #fff;
    padding: 20px;
    color:#918b8b;
  }
  .card-three-items {
    background: #fff;
    display: flex;
    flex-wrap:wrap;
    justify-content:space-between;
    gap:20px;

    
  }
  .card-three-items > div {
    background: #f6f6f6;
    flex:1;

  }
  @media(max-width:1081px){
    .aside {
      display: none;
    }
    .datagrid {
      overflow:auto !important;
    }
  }
`;
