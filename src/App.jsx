import { useEffect, useState } from 'react'
import {Container, Row, Col, Stack} from 'react-bootstrap'
import axios from 'axios';
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { MainContainer } from './MainContainer';
import { List, AppWindow, Airplay, CaretDown, Book, Student} from "@phosphor-icons/react";
import GenderChart from './Components/GenderChart'
import AgeChart from './Components/AgeChart';
import CountryChart from './Components/CountryChart';
import SpecificUser from './Components/SpecificUsers';
function App() {
  const [userData, setUserData] = useState([]);

  const [active, setActive] = useState(true);


  useEffect(() => {
    axios.get('https://randomuser.me/api/?results=500')
      .then(response => {
        const userData = response.data.results;
        setUserData(userData);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);
  

  return (
    <MainContainer>
    <Container fluid className=''>
      <Row>
        <aside className='aside'>
          <div>
              <nav>
                <ul>
                <li>
                    <a href="#">
                    <span>LOGO</span>
                    </a>
                  </li>
                  <li>
                    <a href="#" style={{backgroundColor: active ? '#f6f6f6' : ''}}>
                    <List color={`${active ? '#999':'#fff'}`} size={32} />
                    <span style={{color: active ? '#999' : ''}}>Dashboard</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                    <Airplay size={32} color="#fff" />
                    <span>KPIs</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                    <AppWindow size={32} color="#fff" />
                    <span>Instituitions</span>
                    </a>
                  </li>
                </ul>
              </nav>
          </div>
        </aside>
        <Col className='m-0 main-box'>
          <Row>
          {/* <Stack direction="horizontal" gap={3}>
            <div className="">
              <h2>KPI Dashboard</h2>
            </div>
            <div className="bg-warning border ms-auto">Second item</div>
            <div className="bg-warning border">Third item</div>
          </Stack> */}
          <div className='header-main'>
            <div>
               <h2>KPI Dashboard</h2>
            </div>
            <div className='selects-header'>
                <div style={{backgroundColor:'#828f9d', color:'#fff'}}><span>Institute</span> <CaretDown size={16} color="#ffffff" weight="bold" /></div>
                <div  style={{backgroundColor:'#fff', color:'#999'}}><span>2017 - 2018</span> <CaretDown size={16} color="#999" weight="bold" /></div>
            </div>
          </div>
          </Row>
          <Row>
            <div className='card-main'>
                <div className='card-main-item'>
                <Book size={45} color="#918b8b" weight="bold" />
                <div className='card-main-item-text'>
                  <p>Current Students</p>
                  <span>2,1119</span>
                </div>
                </div>
                <div className='card-main-item'>
                <Student size={45} color="#918b8b" weight="bold" />
                <div className='card-main-item-text'>
                  <p>Current Students</p>
                  <span>2,1119</span>
                </div>
                </div>
                <div className='card-main-item'>
                <List size={45} color="#918b8b" weight="bold" />
                <div className='card-main-item-text'>
                  <p>Current Students</p>
                  <span>2,1119</span>
                </div>
                </div>
                <div className='card-main-item'>
                <Book size={45} color="#918b8b" weight="bold" />
                <div className='card-main-item-text'>
                  <p>Current Students</p>
                  <span>2,1119</span>
                </div>
                </div>
                <div className='card-main-item'>
                <Book size={45} color="#918b8b" weight="bold" />
                <div className='card-main-item-text'>
                  <p>Current Students</p>
                  <span>2,1119</span>
                </div>
                </div>
                <div className='card-main-item'>
                <Book size={45} color="#918b8b" weight="bold" />
                <div className='card-main-item-text'>
                  <p>Current Students</p>
                  <span>2,1119</span>
                </div>
                </div>
            </div>
          </Row>




          <Row>
              <div className='card-main card-two-separate'>
                <div className='card-main-item'>
                <GenderChart  data={userData} /> 
                </div>
                <div className='card-main-item'>
                <AgeChart data={userData} />
                </div>
                {/* <div className='card-main-item'>
                <CountryChart data={userData}/>
                </div> */}


                {/* <div className='card-main-item separate'>
                  <div style={{padding:'20px'}}>
                      <h5>Attendance</h5>
                      <div className='separate-items'>
                          <div>one</div>
                          <div>one</div>
                          <div>one</div>
                      </div>
                  </div>
                  <div style={{padding:'20px'}}>
                      <h5>Attendance</h5>
                      <div className='separate-items'>
                          <div>one</div>
                          <div>one</div>
                          <div>one</div>
                      </div>
                  </div>
                </div> */}
            </div>
          </Row>


          <Row>
              <div className='card-main card-two-separate' style={{overflow:'auto', display:'flex', flexWrap:'wrap'}}>
                <div className='card-main-item' style={{ width: '100%', height: '100%', overflow:'auto',  flexWrap:'wrap'}}>
                <CountryChart data={userData}/>
                </div>


                <div className='card-main-item separate'>
                  <div style={{padding:'20px'}}>
                      <h5>Attendance</h5>
                      <div className='separate-items'>
                          <div>one</div>
                          <div>one</div>
                          <div>one</div>
                      </div>
                  </div>
                  <div style={{padding:'20px'}}>
                      <h5>Attendance</h5>
                      <div className='separate-items'>
                          <div>one</div>
                          <div>one</div>
                          <div>one</div>
                      </div>
                  </div>
                </div>
            </div>
          </Row>






          <Row>
              <div className='card-main card-two-separate datagrid'>
                <div className='card-main-item'>
                <SpecificUser data={userData} />
                </div>
            </div>
          </Row>



          <Row>
            <div className='card-three'>
              <div>
              <h5>Efficiency</h5>
                <div className='card-three-items'>
                    <div>
                      <p>Sucess Rate</p>
                      <span>88%</span>
                    </div>
                     <div>
                      <p>Sucess Rate</p>
                      <span>88%</span>
                    </div>
                     <div>
                      <p>Sucess Rate</p>
                      <span>88%</span>
                    </div>
                     <div>
                      <p>Sucess Rate</p>
                      <span>88%</span>
                    </div>
                </div>
              </div>
            </div>
          </Row>
        </Col>
      </Row>
    </Container>
    </MainContainer>
  )
}

export default App
