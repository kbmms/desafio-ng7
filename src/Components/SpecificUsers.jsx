import React from 'react';

import { DataGrid } from '@mui/x-data-grid';


const SpecificUser = ({ data }) => {
  // Filtrar os dados para exibir apenas as mulheres com 18 anos ou mais
  const filteredData = data.filter(
    (user) => user.gender === 'female' && user.dob.age >= 18
  );

  const columns = [
    { field: 'name', headerName: 'Name', width: 200 },
    { field: 'age', headerName: 'Age', width: 100 },
    { field: 'gender', headerName: 'Gender', width: 120 },
    { field: 'email', headerName: 'Email', width: 300 },
    { field: 'phone', headerName: 'Phone', width: 150 },
  ];


  const rows = filteredData.map((user) => ({
    id: user.login.uuid,
    name: `${user.name.first} ${user.name.last}`,
    age: user.dob.age,
    gender: user.gender,
    email: user.email,
    phone: user.phone,
  }));

  return (
    <div style={{ width: '100%' }}>
        <h2 style={{fontSize:'24px', color:'#918b8b', marginBottom:'20px', padding:'10px 20px'}}>Mulheres acima de 18 anos</h2>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 5 },
          },
        }}
        pageSizeOptions={[5, 10]}
        checkboxSelection
        autoHeight
        disableExtendRowFullWidth
        disableColumnMenu
        disableColumnSelector
        disableDensitySelector
        disableColumnFilter
      />
    </div>
  );
};

export default SpecificUser;
